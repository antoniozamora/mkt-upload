import { Component } from "react";
import axios from "axios";

import styles from './file-uploads.module.css'

export class FileUploads extends Component<any, any> {

    state: any = {
        files: [],
        checkedItems: new Map()
    }

    // 🎨 Render

    render = () => {
        return (
            <div className={'files-container'}>

                <div>
                    {this.showFiles()}
                </div>

                <h1>File Uploads</h1>
                <div>
                    <input type="file" onChange={this.onFileChange} multiple/>
                    <button onClick={this.onFileUpload}>
                        Upload!
                    </button>
                </div>
                {this.fileDataList()}
            </div>
        );
    }

    fileDataList = () => {

        if (this.state.files.length > 0) {

            const listItems = Array.from(this.state.files).map((file: any) =>
                <div key={file.name}>
                    <h4>File Details:</h4>
                    <p>File Name: {file.name}</p>
                    <p>File Type: {file.type}</p>
                    <p>
                        Last Modified:{" "}
                        {file.lastModifiedDate.toDateString()}
                    </p>
                    <img src={file.preview}/>
                </div>
            );

            return (
                <div>
                    {listItems};
                </div>
            );


        } else {
            return (
                <div>
                    <br/>
                    <h4>Choose before Pressing the Upload button</h4>
                </div>
            );
        }
    };

    fileRowDescription = (file: any) => {
        const row = (
            <tr key={file.Key}>
                <td><img src={file.Location} width={100}/></td>
                <td>{file.Key}</td>
                <td>{file.LastModified}</td>
                <td>
                    <input
                        name={file.Key}
                        type="checkbox"
                        checked={this.state.checkedItems.get(file.Key)}
                        onChange={this.handleChange}/>
                </td>
            </tr>
        );

        return row;
    }

    showFiles = () => {

        const objectList: any = this.state.objectList;

        const rows: any = objectList?.map((fileObject: any) => {
            console.log(fileObject);
            return this.fileRowDescription(fileObject);
        });

        if (!rows || rows.length <= 0) return [];

        return (
            <div>
                <h1>Current Files</h1>
                <table className={styles.objectListContainer}>
                    <thead>
                    <tr>
                        <th>File</th>
                        <th>Date</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    {rows}
                    </tbody>
                </table>
                <button onClick={this.onFileDelete}>Delete Files</button>
            </div>
        );
    }

    // 🧰 Data

    componentDidMount() {
        this.getFiles();
    }

    handleChange = (event: any) => {
        const item = event.target.name;
        const isChecked = event.target.checked;
        this.setState((prevState: any) => ({checkedItems: prevState.checkedItems.set(item, isChecked)}));
    }

    onFileChange = (event: any) => {
        // Update the state
        this.setState({files: event.target.files});
    };

    onFileUpload = async () => {

        // Create an object of formData which is used to send the file to the server
        const formData = new FormData();

        // Update the formData object
        Array.from(this.state.files).forEach((file: any) => {
            formData.append("files", file, file.name);
        });

        // Details of the uploaded files
        console.log('Files: ', this.state.files);

        // Request made to the backend api
        // Send formData object
        const res = await axios.post("http://localhost:3000/files/upload", formData);

        console.log('upload done', res);
        this.state.files = [];
        this.getFiles();
    };

    onFileDelete = async () => {

        const itemsToRemove = [...this.state.checkedItems.keys()];
        console.log('items to remove: ', itemsToRemove);

        const res: any = await axios.delete("http://localhost:3000/files", {data: {itemsToRemove}});
        console.log('Res: ', res);
        this.getFiles();

    }

    getFiles = async () => {
        const res: any = await axios.get("http://localhost:3000/files");
        this.setState({objectList: res.data.Contents});
    }
}
