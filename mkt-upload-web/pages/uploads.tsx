import { NextPage } from "next";
import styles from "../styles/uploads.module.css";
import Head from "next/head";
import Image from "next/image";
import { FilePosition } from "postcss";
import { FileUploads } from "../components/file-uploads";


const Uploads: NextPage = () => {
    return (
        <div className={styles.container}>
            <Head>
                <title>File Uploads</title>
                <meta name="description" content="Partner Uploads"/>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <main className={styles.main}>
                <FileUploads></FileUploads>
            </main>
        </div>
    )
}

export default Uploads;
