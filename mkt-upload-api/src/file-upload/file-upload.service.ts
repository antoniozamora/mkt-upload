import { Injectable } from '@nestjs/common';
import * as AWS from "aws-sdk";

@Injectable()
export class FileUploadService {

    prefixFolder = 'images/';
    // prefixFolder = '';

    AWS_S3_BUCKET = 'ganchrowpartners';
    // AWS_S3_BUCKET = 'ganchrow.uploads';
    

    s3 = new AWS.S3
        ({
            accessKeyId: process.env.awsAccessKeyId,
            secretAccessKey: process.env.awsSecretAccessKey,
        });

        // ({
        //     accessKeyId: 'AKIA5SQV4ZVIS2ZEJ3ON',
        //     secretAccessKey: '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',
        // });

    async getFiles() {

        try {

            const params = {
                Bucket: this.AWS_S3_BUCKET,
                MaxKeys: 10,
                Prefix: this.prefixFolder,
            };
    
            const res = await this.s3.listObjects(params).promise();
    
            // ⚠️ the follow snippet retrieves the body of the image if necesary
            // for(let obj of res.Contents) {
            //     if(obj.Size <= 0) continue;
            //     const params = {
            //         Bucket: this.AWS_S3_BUCKET,
            //         Key: obj.Key,
            //     };
            //     const objData = await this.s3.getObject(params).promise();
            //     console.log('🧰 objData', objData);
            // }
    
            // remove the first item 
    
            for (let obj of res.Contents) {
                (obj as any).Location = `https://s3.amazonaws.com/${this.AWS_S3_BUCKET}/${obj.Key}`;
            }
    
            console.log('🧰 res', res);
    
            return res;

        } catch(e) {
            console.log('🔥', e);
        }

    }

    async uploadFile(file, buffer) {

        const { originalname } = file;

        return await this.s3_upload(buffer, this.AWS_S3_BUCKET, originalname, file.mimetype);
    }

    async deleteFiles(fileKeys) {

        const params = {
            Bucket: this.AWS_S3_BUCKET,
            Delete: {
                Objects: fileKeys.map(key => ({ Key: key })),
            }
        };

        const res = await this.s3.deleteObjects(params).promise();
        console.log('🧰 res', res);

        return res;
    }

    async s3_upload(file, bucket, name, mimetype) {
        const params = {
            Bucket: bucket,
            Key: this.prefixFolder + name,
            Body: file,
            ACL: "public-read",
            ContentType: mimetype,
            ContentDisposition: "inline",
            CreateBucketConfiguration:
            {
                LocationConstraint: "ap-south-1"
            },
        };

        console.log('uploading file to s3 ' + name);

        try {
            let s3Response = await this.s3.upload(params).promise();

            return s3Response;
        }
        catch (e) {
            console.log(e);
        }
    }

}
