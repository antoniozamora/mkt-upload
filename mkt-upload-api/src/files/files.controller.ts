import { Body, Controller, Delete, Get, Post, Req, UploadedFile, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { Request } from "express";
import { FileUploadService } from 'src/file-upload/file-upload.service';
import * as sharp from "sharp";

@Controller('files')
export class FilesController {

    constructor(private fileUploadService: FileUploadService) { }

    @Post('upload')
    @UseInterceptors(FilesInterceptor('files'))
    async uploadFile(@UploadedFiles() files: any, @Req() request: Request) {

        const res = await Promise.all(files.map(async (file) => this.processImages(file)));
        return res;
    }

    @Get()
    async getPartnerFiles() {

        const res = await this.fileUploadService.getFiles();
        console.log('🧰 res', res);

        return res;

    }

    @Delete()
    async deletePartnerFiles(@Req() request: Request) {

        const files = request.body.itemsToRemove;
        const res = await this.fileUploadService.deleteFiles(files);
        console.log('🧰 res', res);

        return res;

    }

    async processImages(file) {

        // resize image ⚠️
        const resizedImageBuffer = await sharp(file.buffer).resize(300, 300, {
            fit: 'contain',
            background: { r: 0, g: 0, b: 0, alpha: 0 }
        }).toBuffer();

        const res = await this.fileUploadService.uploadFile(file, resizedImageBuffer);
        console.log('🧰 res', res);

        return res;
    }

}
